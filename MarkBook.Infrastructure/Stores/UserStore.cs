using Markbook.Infrastructure;
using MarkBook.Core.Domain.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MarkBook.Infrastructure.Stores
{
   public class UserStore : IUserPasswordStore<ApplicationUser>, IUserEmailStore<ApplicationUser>
   {
      private readonly MarkBookDbContext _context;
      private DbSet<ApplicationUser> _items { get; }
      public UserStore(MarkBookDbContext context)
      {
         _context = context;
         _items = context.ApplicationUsers;
      }
      public Task CreateAsync(ApplicationUser user)
      {
         throw new NotImplementedException();
      }


      public async Task<IdentityResult> CreateAsync(ApplicationUser user,
          CancellationToken cancellationToken = default(CancellationToken))
      {
         try
         {
            await _items.AddAsync(user, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
         }
         catch (DbUpdateException)
         {
            return IdentityResult.Failed(new IdentityError { Description = $"Could not create User {user.Email}" });
         }
         return IdentityResult.Success;
      }

      public Task DeleteAsync(ApplicationUser user)
      {
         throw new NotImplementedException();
      }

      public Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }

      public void Dispose()
      {
      }

      public Task<ApplicationUser> FindByIdAsync(string userId,
          CancellationToken cancellationToken = default(CancellationToken))
      {
         if (!Guid.TryParse(userId, out var guidId))
         {
            throw new ArgumentException($"{userId} is not valid Id");
         }

         return _items.FirstAsync(x => x.Id == guidId);
      }

      public Task<ApplicationUser> FindByNameAsync(string userName)
      {
         if (userName == null) throw new ArgumentNullException(nameof(userName));

         return _context.ApplicationUsers.SingleOrDefaultAsync(x => x.Name == userName);
      }

      public async Task<ApplicationUser> FindByNameAsync(string normalizedUserName,
          CancellationToken cancellationToken = default(CancellationToken))
      {
         if (normalizedUserName == null) throw new ArgumentNullException(nameof(normalizedUserName));

         return await _context.ApplicationUsers.SingleOrDefaultAsync(x => x.Name == normalizedUserName);
      }

      public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         return Task.FromResult(user.Name);
      }

      public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));

         return Task.FromResult(user.PasswordHash);
      }

      public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));

         return Task.FromResult(user.Id.ToString());
      }

      public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));

         return Task.FromResult(user.Name);
      }

      public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }

      public Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
      {
         return Task.CompletedTask;
      }

      public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));
         if (passwordHash == null) throw new ArgumentNullException(nameof(passwordHash));

         user.PasswordHash = passwordHash;
         return Task.FromResult<object>(null);
      }

      public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }


      public Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }

      public Task SetEmailAsync(ApplicationUser user, string email, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));
         if (email == null) throw new ArgumentNullException(nameof(email));
         user.Email = email;
         return Task.FromResult<object>(null);
      }

      public Task<string> GetEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         if (user == null) throw new ArgumentNullException(nameof(user));
         return Task.FromResult(user.Email);
      }

      public Task<bool> GetEmailConfirmedAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }

      public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed, CancellationToken cancellationToken)
      {
         throw new NotImplementedException();
      }

      public async Task<ApplicationUser> FindByEmailAsync(string normalizedEmail,
          CancellationToken cancellationToken = default(CancellationToken))
      {
         if (normalizedEmail == null) throw new ArgumentNullException(nameof(normalizedEmail));
         return await _context.ApplicationUsers.SingleOrDefaultAsync(x => x.Email.ToUpper() == normalizedEmail, cancellationToken);
      }

      public Task<string> GetNormalizedEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
      {
         return Task.FromResult(user.Email);
      }

      public Task SetNormalizedEmailAsync(ApplicationUser user, string normalizedEmail, CancellationToken cancellationToken)
      {
         return Task.CompletedTask;
      }
   }
}
