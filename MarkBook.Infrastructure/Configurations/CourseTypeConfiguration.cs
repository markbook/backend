using MarkBook.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarkBook.Infrastructure.Configurations
{
   public class CourseTypeConfiguration : IEntityTypeConfiguration<Course>
   {
      public void Configure(EntityTypeBuilder<Course> builder)
      {
         builder.ToTable("courses");

         builder.HasKey(x => x.Id);


         builder.Property(x => x.Name).IsRequired();

         builder.HasAlternateKey(x => x.Name)
                .HasName("UX_name_courses");

      }
   }
}
