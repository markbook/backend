using MarkBook.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarkBook.Infrastructure.Configurations
{
   public class RoleTypeConfiguration : IEntityTypeConfiguration<ApplicationRole>
   {
      public void Configure(EntityTypeBuilder<ApplicationRole> builder)
      {
         builder.ToTable("roles");

         builder.HasKey(x => x.Id);

         builder.Property(x => x.Id)
             .HasColumnName("id");

         builder.Property(x => x.Name)
             .HasColumnName("name")
             .IsRequired();

         builder.HasAlternateKey(x => x.Name);
      }
   }
}
