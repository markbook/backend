using MarkBook.Core.Domain.Model.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace MarkBook.Infrastructure.Configurations
{
   public class UserTypeConfiguration : IEntityTypeConfiguration<User>
   {
      public void Configure(EntityTypeBuilder<User> builder)
      {
         builder.ToTable("users");

         builder.HasKey(x => x.Id);

         builder.HasAlternateKey(x => x.Email)
             .HasName("UX_email_users");

         builder.Property(x => x.Email)
             .IsRequired();

         builder.Property(x => x.CreateDate)
                .HasDefaultValue(DateTime.Now)
                .ValueGeneratedOnAdd();

         builder.Property(x => x.LastName)
             .IsRequired();

         builder.Property(x => x.FirstName)
             .IsRequired();

         builder.Property(x => x.MiddleName)
             .IsRequired();
      }
   }
}
