using MarkBook.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarkBook.Infrastructure.Configurations
{
   public class UserCourseTypeConfiguration : IEntityTypeConfiguration<UserCourse>
   {
      public void Configure(EntityTypeBuilder<UserCourse> builder)
      {
         builder.ToTable("user_courses");

         builder.HasKey(x => x.CourseId);

         builder.HasAlternateKey(x => new { x.UserId, x.CourseId })
             .HasName("UX_user_course_user_id_course_id");

         builder.Property(x => x.UserId)
             .IsRequired();

         builder.Property(x => x.CourseId)
                .IsRequired();

         builder.HasOne(x => x.User)
             .WithMany()
             .HasForeignKey(x => x.UserId)
             .HasConstraintName("FK_user_course_user");

         builder.HasOne(x => x.Course)
             .WithMany()
             .HasForeignKey(x => x.CourseId)
             .HasConstraintName("FK_user_course_course");
      }
   }
}
