using MarkBook.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarkBook.Infrastructure.Configurations
{
   class ApplicationUserTypeConfiguration : IEntityTypeConfiguration<ApplicationUser>
   {
      public void Configure(EntityTypeBuilder<ApplicationUser> builder)
      {
         builder.ToTable("application_users");

         builder.HasKey(x => x.Id);

         builder.HasAlternateKey(x => x.Email)
                .HasName("UX_email_application_users");

         builder.HasAlternateKey(x => x.Name)
             .HasName("UX_name_application_users");

         builder.Property(x => x.PasswordHash)
             .IsRequired();
      }
   }
}
