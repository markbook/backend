using MarkBook.Core.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MarkBook.Infrastructure.Configurations
{
   public class LessonTypeConfiguration : IEntityTypeConfiguration<Lesson>
   {
      public void Configure(EntityTypeBuilder<Lesson> builder)
      {
         builder.ToTable("lessons");

         builder.HasKey(x => x.Id);

         builder.Property(x => x.CourseId)
             .IsRequired();

         builder.Property(x => x.TeacherId)
             .HasColumnName("teacher_id");

         builder.HasOne(x => x.Teacher)
             .WithMany()
             .HasForeignKey(x => x.TeacherId)
             .HasConstraintName("FK_lesson_user");

         builder.HasOne(x => x.Course)
             .WithMany(x => x.Lessons)
             .HasForeignKey(x => x.CourseId)
             .HasConstraintName("FK_lesson_course");

         builder.Property(x => x.Description)
             .IsRequired();

         builder.Property(x => x.Name)
                .IsRequired();

      }
   }
}
