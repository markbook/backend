﻿using MarkBook.Core.Domain.Model;
using MarkBook.Core.Domain.Model.User;
using Microsoft.EntityFrameworkCore;

namespace Markbook.Infrastructure
{
   public class MarkBookDbContext : DbContext
   {
      public DbSet<User> Users { get; set; }

      public DbSet<ApplicationUser> ApplicationUsers { get; set; }

      public DbSet<Course> Courses { get; set; }

      public DbSet<UserCourse> UserCourses { get; set; }
      public MarkBookDbContext(DbContextOptions options) : base(options)
      {
         Database.EnsureCreated();
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.ApplyConfigurationsFromAssembly(typeof(MarkBookDbContext).Assembly);
         base.OnModelCreating(modelBuilder);
      }
   }
}
