using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBook.Infrastructure.Services
{
   public enum ErrorCode
   {
      /// <summary>
      /// Доступ запрещен
      /// </summary>
      AccessDenied = 1,

      /// <summary>
      /// Объект не найден
      /// </summary>
      NotFound = 2,

      /// <summary>
      /// Неправильный формат данных
      /// </summary>
      BadRequest = 3,

   }
}
