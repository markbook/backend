using System;
using System.Collections.Generic;

namespace MarkBook.Infrastructure.Services
{
   public class ErrorResult
   {
      public ErrorResult(ErrorCode code, List<string> messages = null)
      {
         Error = code;
         Messages = messages ?? new List<string>();
      }
      public ErrorCode Error { get; set; }
      public List<string> Messages { get; set; }

      public int GetStatusCode()
      {
         return Error switch
         {
            ErrorCode.BadRequest => 400,
            ErrorCode.AccessDenied => 403,
            ErrorCode.NotFound => 404,
            _ => throw new ArgumentOutOfRangeException(nameof(ErrorCode)),
         };
      }
   }
}
