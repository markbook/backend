using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkBook.Core.Domain.Model;
using MarkBook.Core.Domain.Model.User;

namespace MarkBook.Infrastructure.Services.AuthService
{
   /// <summary>
   /// Сервис аутентификации
   /// </summary>
   public interface IAuthService : ITransientService
   {
      /// <summary>
      /// 
      /// </summary>
      /// <param name="user"></param>
      /// <param name="password"></param>
      /// <returns></returns>
      Task<Result<ApplicationUser>> RegisterUserAsync(User user, string password);

      /// <summary>
      /// 
      /// </summary>
      /// <param name="email"></param>
      /// <param name="password"></param>
      /// <returns></returns>
      Task<Result<ApplicationUser>> LoginUserAsync(string email, string password);
   }
}
