using MarkBook.Core.Domain.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkBook.Core.Domain.Model;
using Markbook.Infrastructure;
using Microsoft.AspNetCore.Identity;

namespace MarkBook.Infrastructure.Services.AuthService
{
   public class AuthService : IAuthService
   {
      private readonly UserManager<ApplicationUser> _userManager;
      private readonly MarkBookDbContext _context;
      private readonly SignInManager<ApplicationUser> _signInManager;

      public AuthService(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
         MarkBookDbContext context)
      {
         _context = context;
         _userManager = userManager;
         _signInManager = signInManager;
      }

      public async Task<Result<ApplicationUser>> LoginUserAsync(string email, string password)
      {
         var user = await _userManager.FindByEmailAsync(email);
         if (user == null)
            return new Result<ApplicationUser>(ErrorCode.BadRequest).WithMessage("Неверное имя пользователя или пароль");
         var result = await _signInManager.PasswordSignInAsync(user, password, false, false);
         if (!result.Succeeded) 
            return new Result<ApplicationUser>(ErrorCode.BadRequest).WithMessage("Неверное имя пользователя или пароль");
         return Result<ApplicationUser>.Ok(user);
      }

      public async Task<Result<ApplicationUser>> RegisterUserAsync(User user, string password)
      {
         var applicationUser = new ApplicationUser()
         {
            Email = user.Email,
            Name = user.Login
         };
         var result = await _userManager.CreateAsync(applicationUser, password);
         if (!result.Succeeded)
            return new Result<ApplicationUser>(ErrorCode.BadRequest);
         user.Id = applicationUser.Id;
         await _context.Users.AddAsync(user);
         await _context.SaveChangesAsync();
         return Result<ApplicationUser>.Ok(applicationUser);
      }
   }
}
