using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkBook.Infrastructure.Services.AuthService;

namespace MarkBook.Infrastructure.Services
{
   public class None
   {
      private None()
      {
      }
   }

   public struct Result<T>
   {
      public Result(ErrorCode error, T value)
      {
         Error = new ErrorResult(error);
         Value = value;
      }

      public Result(T value)
      {
         Error = null;
         Value = value;
      }

      public Result(ErrorCode error)
      {
         Error = new ErrorResult(error);
         Value = default;
      }
      public static Result<T> Ok(T value)
      {
         return new Result<T>(value);
      }

      public static Result<T> Ok()
      {
         return new Result<T>(default(T));
      }

      public ErrorResult Error { get; set; }

      public T Value { get; }

      public bool IsSuccess => Error == null;

      public Result<T> WithMessage(string message)
      {
         Error?.Messages.Add(message);
         return this;
      }
   }
}
