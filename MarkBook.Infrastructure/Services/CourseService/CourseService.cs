using MarkBook.Core.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Markbook.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace MarkBook.Infrastructure.Services.CourseService
{
   public class CourseService : ICourseService
   {
      private readonly MarkBookDbContext _context;
      public CourseService(MarkBookDbContext context)
      {
         _context = context;
      }
      public async Task<Result<Course>> AddCourse(Course course)
      {
         if (await _context.Courses.AnyAsync(x => x.Name == course.Name))
            return new Result<Course>(ErrorCode.BadRequest)
               .WithMessage($"Курс с названием {course.Name} уже существует");
         await _context.Courses.AddAsync(course);
         await _context.SaveChangesAsync();
         return Result<Course>.Ok(course);
      }

      public async Task<Result<None>> AllowUserToTakeCourse(Guid courseId, Guid userId)
      {
         if (_context.Courses.SingleOrDefault(x => x.Id == courseId) == null)
               return new Result<None>(ErrorCode.BadRequest);
         await _context.UserCourses.AddAsync(new UserCourse() { CourseId = courseId, UserId = userId });
         await _context.SaveChangesAsync();
         return Result<None>.Ok();
      }
   }
}
