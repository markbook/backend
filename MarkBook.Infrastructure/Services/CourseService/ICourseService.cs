using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarkBook.Core.Domain.Model;

namespace MarkBook.Infrastructure.Services.CourseService
{
   /// <summary>
   /// 
   /// </summary>
   public interface ICourseService : ITransientService
   {
      /// <summary>
      /// 
      /// </summary>
      /// <param name="course"></param>
      /// <returns></returns>
      Task<Result<Course>> AddCourse(Course course);


      /// <summary>
      /// 
      /// </summary>
      /// <param name="courseId"></param>
      /// <param name="userId"></param>
      /// <returns></returns>
      Task<Result<None>> AllowUserToTakeCourse(Guid courseId, Guid userId);
   }
}
