﻿using Markbook.Infrastructure;
using MarkBook.Core.Domain.Model.User;
using MarkBook.Web.Extensions;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace MarkBook.Web.Middlewares
{
   public class UserMiddleware
   {
      private readonly RequestDelegate _next;

      public UserMiddleware(RequestDelegate next)
      {
         _next = next;
      }

      public async Task InvokeAsync(HttpContext httpContext, MarkBookDbContext dbContext)
      {
         var user = await dbContext.Users.FindAsync(httpContext.GetUserId());
         httpContext.Items.Add(nameof(User), user);
         await _next.Invoke(httpContext);
      }
   }
}
