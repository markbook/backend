using AutoMapper;
using Markbook.Infrastructure;
using MarkBook.Core.Domain.Model;
using MarkBook.Core.Domain.Model.User;
using MarkBook.Web.Dtos.Request;
using MarkBook.Web.Dtos.Responses;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using MarkBook.Infrastructure.Services.AuthService;

namespace MarkBook.Web.Controllers
{
   /// <summary>
   /// Контроллер аунтификации пользователей
   /// </summary>
   [ApiController]
   [Route("/auth")]
   [Produces("application/json")]
   public class AuthController : ControllerBase
   {
      private readonly IAuthService _authService;
      private readonly IOptions<AuthOptions> _authOptions;
      private readonly IMapper _mapper;
      public AuthController(
          IAuthService authService,
          IOptions<AuthOptions> authOptions,
          MarkBookDbContext context,
          IMapper mapper)
      {
         _authService = authService;
         _authOptions = authOptions;
         _mapper = mapper;
      }

      /// <summary>
      /// Регистрация пользователя
      /// </summary>
      /// <param name="request">request</param>
      /// <returns></returns>
      [HttpPost]
      [Route("/register")]
      public async Task<ActionResult> Register([FromForm] RegisterRequest request)
      {
         var result = await _authService.RegisterUserAsync(_mapper.Map<User>(request), request.Password);
         return result.IsSuccess ? Ok() : StatusCode(result.Error.GetStatusCode());
      }

      /// <summary>
      /// Login
      /// </summary>
      /// <param name="request"></param>
      /// <returns></returns>
      [HttpPost]
      [Route("/login")]
      public async Task<ActionResult<LoginResponse>> Login([FromForm] LoginRequest request)
      {
         var result =  await _authService.LoginUserAsync(request.Email, request.Password);
         return result.IsSuccess
            ? Ok(new LoginResponse {AccessToken = GenerateJwtToken(result.Value), ExpiresIn = _authOptions.Value.TokenLifetime})
            : StatusCode(result.Error.GetStatusCode(), new {result.Error.Messages});
      }

      private string GenerateJwtToken(ApplicationUser user)
      {
         var authParams = _authOptions.Value;

         var securityKey = authParams.GetSymmetricSecurityKey();
         var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

         var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
            };

         var token = new JwtSecurityToken(authParams.Issuer,
             authParams.Audience,
             claims,
             expires: DateTime.Now.AddSeconds(authParams.TokenLifetime),
             signingCredentials: credentials);

         return new JwtSecurityTokenHandler().WriteToken(token);
      }
   }
}
