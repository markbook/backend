using AutoMapper;
using Markbook.Infrastructure;
using MarkBook.Core.Domain.Model;
using MarkBook.Web.Dtos.Request;
using MarkBook.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using MarkBook.Infrastructure.Services.CourseService;

namespace MarkBook.Web.Controllers
{
   /// <summary>
   /// Контроллер для работы с курсами
   /// </summary>
   [ApiController]
   [Route("courses/")]
   [Produces("application/json")]
   public class CourseController : ControllerBase
   {
      private readonly IMapper _mapper;
      private readonly ICourseService _courseService;

      public CourseController(IMapper mapper, ICourseService courseService)
      {
         _courseService = courseService;
         _mapper = mapper;
      }

      /// <summary>
      /// Добавить курс в систему
      /// </summary>
      /// <param name="request">Запрос на добавление курса</param>
      /// <returns></returns>
      [Authorize]
      [HttpPost("add")]
      public async Task<IActionResult> AddCourse([FromBody] AddCourseRequest request)
      {
         var course = _mapper.Map<Course>(request);
         var result = await _courseService.AddCourse(course);
         return result.IsSuccess
            ? Ok(string.Empty)
            : StatusCode(result.Error.GetStatusCode(), new {result.Error.Messages});
      }

      /// <summary>
      /// Подписаться на курс
      /// </summary>
      /// <param name="id">Id курса</param>
      /// <returns></returns>
      [Authorize]
      [HttpPost("{id:guid}/subscribe")]
      [ProducesResponseType(StatusCodes.Status200OK)]
      [ProducesResponseType(StatusCodes.Status404NotFound)]
      public async Task<IActionResult> AllowUserToTakeCourse([FromRoute] Guid id)
      {
         var userId = HttpContext.GetUserId();
         var result = await _courseService.AllowUserToTakeCourse(id, userId);
         return result.IsSuccess
            ? Ok(string.Empty)
            : StatusCode(result.Error.GetStatusCode(), new { result.Error.Messages });
      }
   }
}
