using Markbook.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using MarkBook.Web.Extensions;

namespace MarkBook.Web.Controllers
{
   [ApiController]
   [Route("[controller]")]
   public class WeatherForecastController : ControllerBase
   {
      private static readonly string[] Summaries = new[]
      {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

      private readonly ILogger<WeatherForecastController> _logger;
      private readonly MarkBookDbContext _context;

      public WeatherForecastController(ILogger<WeatherForecastController> logger, MarkBookDbContext context)
      {
         _logger = logger;
         _context = context;
      }

      [Authorize]
      [HttpGet]
      public IEnumerable<WeatherForecast> Get()
      {
         var rng = new Random();
         var user = HttpContext.GetUser();
         return Enumerable.Range(1, 5).Select(index => new WeatherForecast
         {
            Date = DateTime.Now.AddDays(index),
            TemperatureC = rng.Next(-20, 55),
            Summary = Summaries[rng.Next(Summaries.Length)]
         })
             .ToArray();
      }
   }
}
