﻿using System.ComponentModel.DataAnnotations;

namespace MarkBook.Web.Dtos.Request
{
   public class LoginRequest
   {
      /// <summary>
      /// email 
      /// </summary>
      [Required]
      public string Email { get; set; }

      /// <summary>
      /// пароль
      /// </summary>
      [Required]
      public string Password { get; set; }
   }
}
