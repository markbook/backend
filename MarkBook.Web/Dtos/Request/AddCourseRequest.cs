﻿namespace MarkBook.Web.Dtos.Request
{
   public class AddCourseRequest
   {
      /// <summary>
      /// Название курса
      /// </summary>
      public string Name { get; set; }

      /// <summary>
      /// Описание курса
      /// </summary>
      public string Description { get; set; }
   }
}
