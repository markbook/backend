﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace MarkBook.Web.Dtos.Request
{
   public class RegisterRequest
   {
      /// <summary>
      /// email 
      /// </summary>
      [FromForm]
      [Required]
      public string Email { get; set; }

      /// <summary>
      /// пароль
      /// </summary>
      [FromForm]
      [Required]
      public string Password { get; set; }

      /// <summary>
      /// Логин
      /// </summary>
      [FromForm]
      [Required]
      public string UserName { get; set; }

      /// <summary>
      /// Фамилия
      /// </summary>
      [FromForm]
      [Required]
      public string LastName { get; set; }

      /// <summary>
      /// Имя
      /// </summary>
      [FromForm]
      [Required]
      public string FirstName { get; set; }

      /// <summary>
      /// Отчество
      /// </summary>
      [FromForm]
      [Required]
      public string MiddleName { get; set; }
   }
}
