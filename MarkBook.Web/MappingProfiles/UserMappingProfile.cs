﻿using AutoMapper;
using MarkBook.Core.Domain.Model.User;
using MarkBook.Web.Dtos.Request;

namespace MarkBook.Web.MappingProfiles
{
   public class UserMappingProfile : Profile
   {
      public UserMappingProfile()
      {
         CreateMap<RegisterRequest, User>()
             .ForMember(u => u.Login, opt => opt.MapFrom(x => x.UserName));
      }
   }
}
