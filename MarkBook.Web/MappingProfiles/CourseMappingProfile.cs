﻿using AutoMapper;
using MarkBook.Core.Domain.Model;
using MarkBook.Web.Dtos.Request;

namespace MarkBook.Web.MappingProfiles
{
   public class CourseMappingProfile : Profile
   {
      public CourseMappingProfile()
      {
         CreateMap<AddCourseRequest, Course>();
      }
   }
}
