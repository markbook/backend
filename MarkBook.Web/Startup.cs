using AutoMapper;
using Markbook.Infrastructure;
using MarkBook.Core.Domain.Model;
using MarkBook.Infrastructure.Services;
using MarkBook.Infrastructure.Services.AuthService;
using MarkBook.Infrastructure.Stores;
using MarkBook.Web.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using NJsonSchema.Generation;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace MarkBook.Web
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      public void ConfigureServices(IServiceCollection services)
      {
         AddServices(services);
         services.AddControllers()
             .AddNewtonsoftJson(options =>
             {
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
             });

         services.AddAutoMapper(typeof(Startup));

         var authOptionsConfiguration = Configuration.GetSection("Auth");
         services.Configure<AuthOptions>(authOptionsConfiguration);
         var authOptions = authOptionsConfiguration.Get<AuthOptions>();
         services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
             .AddJwtBearer(options =>
             {
                options.RequireHttpsMetadata = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                   ValidateIssuer = true,
                   ValidIssuer = authOptions.Issuer,

                   ValidateAudience = true,
                   ValidAudience = authOptions.Audience,

                   ValidateLifetime = true,

                   IssuerSigningKey = authOptions.GetSymmetricSecurityKey(),
                   ValidateIssuerSigningKey = true
                };
             });
         AddIdentityCore(services);
         services.AddMvcCore();
         services.AddDbContext<MarkBookDbContext>(opt =>
            opt.UseSqlServer(Configuration.GetConnectionString("MarkBookDbContext"),
                 x => x.MigrationsAssembly(typeof(MarkBookDbContext).Assembly.FullName)));
         services.AddOpenApiDocument(c =>
         {
            c.SerializerSettings = new JsonSerializerSettings
            {
               ContractResolver = new DefaultContractResolver(),
               MissingMemberHandling = MissingMemberHandling.Ignore
            };
            c.DefaultResponseReferenceTypeNullHandling = ReferenceTypeNullHandling.NotNull;
            c.DefaultReferenceTypeNullHandling = ReferenceTypeNullHandling.NotNull;
            c.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT"));
            c.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT", new OpenApiSecurityScheme
            {
               Type = OpenApiSecuritySchemeType.ApiKey,
               Name = "Authorization",
               In = OpenApiSecurityApiKeyLocation.Header,
               Description = "Авторизация: Bearer {JWT token}",
            }));
         });
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }

         app.UseRouting();
         app.UseAuthentication();
         app.UseAuthorization();
         app.UseOpenApi();
         app.UseSwaggerUi3();
         app.UseEndpoints(endpoints =>
         {
            endpoints.MapControllers();
         });
         app.UseUserMiddleware();
      }

      private void AddIdentityCore(IServiceCollection services)
      {
         services.AddTransient<IUserStore<ApplicationUser>, UserStore>();
         services.AddTransient<IRoleStore<ApplicationRole>, RoleStore>();
         services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
             {
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
             })
             .AddUserStore<UserStore>()
             .AddRoleStore<RoleStore>()
             .AddDefaultTokenProviders();
      }

      private void AddServices(IServiceCollection services)
      {
         services.Scan(s => s.FromAssemblyOf<AuthService>()
            .AddClasses(c => c.AssignableTo<ITransientService>())
            .AsImplementedInterfaces()
            .WithTransientLifetime());
      }
   }
}
