﻿using MarkBook.Web.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace MarkBook.Web.Extensions
{
   public static class ApplicationBuilderExtesions
   {
      public static IApplicationBuilder UseUserMiddleware(this IApplicationBuilder builder)
      {
         return builder.UseMiddleware<UserMiddleware>();
      }
   }
}
