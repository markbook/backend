using MarkBook.Core.Domain.Model.User;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace MarkBook.Web.Extensions
{
   public static class HttpContextExtesions
   {
      public static Guid GetUserId(this HttpContext context)
      {
         return Guid.TryParse(context.User.FindFirstValue(ClaimTypes.NameIdentifier), out var result)
            ? result 
            : Guid.Empty;
      }

      public static User GetUser(this HttpContext context)
      {
         if (!context.Items.TryGetValue(nameof(User), out var value))
            return null;
         return (User)value;
      }
   }
}
