﻿using System;

namespace MarkBook.Core.Domain.Model
{
   public class ApplicationRole
   {
      public Guid Id { get; set; }
      public string Name { get; set; }
   }
}
