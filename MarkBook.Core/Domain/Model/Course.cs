﻿using System;
using System.Collections.Generic;

namespace MarkBook.Core.Domain.Model
{
   public class Course
   {
      public Guid Id { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public List<Lesson> Lessons { get; set; }
   }
}
