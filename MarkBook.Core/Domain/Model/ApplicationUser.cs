﻿using System;
using System.Security.Principal;

namespace MarkBook.Core.Domain.Model
{
   public class ApplicationUser : IIdentity
   {
      public Guid Id { get; set; }
      public string Email { get; set; }

      public string PasswordSalt { get; set; }

      public string PasswordHash { get; set; }

      public string AuthenticationType { get; set; }

      public bool IsAuthenticated { get; set; }

      public string Name { get; set; }
   }

}
