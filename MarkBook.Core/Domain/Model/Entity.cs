﻿using System.ComponentModel.DataAnnotations;

namespace MarkBook.Core.Domain.Model
{
   public abstract class Entity<TKey>
   {
      protected Entity(TKey key)
      {
         Key = key;
      }

      protected Entity()
      {
      }
      [Key]
      public TKey Key { get; }
   }
}
