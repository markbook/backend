﻿using System;
using System.Collections.Generic;

namespace MarkBook.Core.Domain.Model.User
{
   public class User
   {
      public Guid Id { get; set; }
      public string LastName { get; set; }

      public string FirstName { get; set; }
      public string MiddleName { get; set; }

      public string FullName => FirstName + " " + MiddleName + " " + LastName;
      public DateTime CreateDate { get; set; }

      public string Email { get; set; }

      public string Login { get; set; }

      public List<UserCourse> UserCourses { get; set; }
   }
}
