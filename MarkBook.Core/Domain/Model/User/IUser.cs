﻿using System;

namespace MarkBook.Core.Domain.Model.User
{
   public interface IUser
   {
      Guid Key { get; }
      //TODO ValueObject
      string LastName { get; }
      string FirstName { get; }
      string MiddleName { get; }
      string Login { get; }
   }
}
