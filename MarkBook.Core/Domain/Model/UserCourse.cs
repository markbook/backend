﻿using System;

namespace MarkBook.Core.Domain.Model
{
   public class UserCourse
   {
      public Guid Id { get; set; }

      public Guid UserId { get; set; }

      public User.User User { get; set; }

      public Guid CourseId { get; set; }

      public Course Course { get; set; }
   }
}
