﻿using System;

namespace MarkBook.Core.Domain.Model
{
   public class Lesson
   {
      public Guid Id { get; set; }

      public string Name { get; set; }

      public string Description { get; set; }

      public int Duration { get; set; }
      public Guid CourseId { get; set; }

      public Course Course { get; set; }

      public DateTime Date { get; set; }

      public Guid? TeacherId { get; set; }

      public User.User Teacher { get; set; }
   }
}
